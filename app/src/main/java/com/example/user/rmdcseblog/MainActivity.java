package com.example.user.rmdcseblog;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.squareup.picasso.Picasso;

import java.io.ByteArrayOutputStream;

public class MainActivity extends AppCompatActivity {

    private RecyclerView mExamList;

    private DatabaseReference mDatabase;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mDatabase = FirebaseDatabase.getInstance().getReference().child("Blog");

        mExamList = (RecyclerView) findViewById(R.id.exam_list);
        mExamList.setHasFixedSize(true);
        mExamList.setLayoutManager(new LinearLayoutManager(this));


    }

    @Override
    protected void onStart() {
        super.onStart();

        FirebaseRecyclerAdapter<Exam, ExamViewHolder> firebaseRecyclerAdapter = new FirebaseRecyclerAdapter<Exam, ExamViewHolder>(

                Exam.class,
                R.layout.exam_row,
                ExamViewHolder.class,
                mDatabase

        ) {
            @Override
            protected void populateViewHolder(ExamViewHolder viewHolder, Exam model, int position) {

                viewHolder.setTitle(model.getTitle());
                viewHolder.setDesc(model.getDesc());
                viewHolder.setImage(getApplicationContext(), model.getImage());

            }
        };

        mExamList.setAdapter(firebaseRecyclerAdapter);

    }

    public static class ExamViewHolder extends RecyclerView.ViewHolder{


        View mView;

        public ExamViewHolder(View itemView) {
            super(itemView);

            mView = itemView;

        }

        public void setTitle(String title){

            TextView post_title = (TextView) mView.findViewById(R.id.post_Title);
            post_title.setText(title);

        }

        public void setDesc(String desc){

            TextView pos_Desc = (TextView) mView.findViewById(R.id.post_Desc);
            pos_Desc.setText(desc);

        }

        public void setImage(Context ctx, String image){

            ImageView post_image = (ImageView) mView.findViewById(R.id.post_Image);
            Picasso.with(ctx).load(image).into(post_image);

        }

    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        getMenuInflater().inflate(R.menu.main_menu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        if(item.getItemId() == R.id.action_add){

            Toast.makeText(this, "Functionality coming soon.", Toast.LENGTH_SHORT).show();

        }

        return super.onOptionsItemSelected(item);
    }
}


