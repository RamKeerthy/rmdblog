package com.example.user.rmdcseblog;

/**
 * Created by user on 3/25/2017.
 */

public class Exam {

    private String desc;
    private String title;
    private String image;

    public Exam(){

    }

    public Exam(String desc, String title, String image) {
        this.desc = desc;
        this.title = title;
        this.image = image;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }
}